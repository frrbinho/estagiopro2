class CreateDisciplinas < ActiveRecord::Migration[5.2]
  def change
    create_table :disciplinas do |t|
      t.string :Nome_disciplina
      t.string :Turma
      t.string :Dias_de_aula
      t.string :Horario

      t.timestamps
    end
  end
end
